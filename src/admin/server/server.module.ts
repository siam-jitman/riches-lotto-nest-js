import { Module } from '@nestjs/common';
import { ServerService } from './server.service';
import { ServerController } from './server.controller';
// import { ServerResponseDTO, ServerResponseWithoutPagingDTO } from './dto/reponse';
import { m_server_info } from '../../utils/db/entities/tables/master/m_server_info';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([m_server_info]),
  ],
  providers: [ServerService],
  controllers: [ServerController]
})
export class ServerModule { }
