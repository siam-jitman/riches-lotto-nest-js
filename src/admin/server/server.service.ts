import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { m_server_info } from '../../utils/db/entities/tables/master/m_server_info'
import { CreateServerInput, DeleteServerInput } from './dto/request';

@Injectable()
export class ServerService {
  private readonly logger = new Logger(ServerService.name);

  constructor(
    @InjectRepository(m_server_info)
    private readonly serverInfoRepository: Repository<m_server_info>,
  ) { }

  async createServer(req: CreateServerInput): Promise<m_server_info> {

    let result = await this.serverInfoRepository.save({
      server_ip: req.ipAddress,
      server_dns: req.domainName,
      avaliable: true
    })

    this.logger.debug("createServer --> result --> ", result)
    return result;
  }

  async deleteServer(req: DeleteServerInput): Promise<DeleteResult> {

    let result = await this.serverInfoRepository.delete({ server_id: req.serverId })

    this.logger.debug("deleteServer --> result --> ", JSON.stringify(result))
    return result;
  }

  async getServerAvaliableOne(): Promise<m_server_info> {

    let result = await this.serverInfoRepository.findOne({
      where: [{ avaliable: true }]
    })

    this.logger.debug("getServerAvaliableOne --> result --> ", JSON.stringify(result))
    return result;
  }

  async updateServerAvaliable(serverId: number, avaliable: boolean): Promise<m_server_info> {

    let result = await this.serverInfoRepository.save({
      server_id: serverId,
      avaliable: avaliable
    })

    this.logger.debug("updateServerAvaliable --> result --> ", JSON.stringify(result))
    return result;
  }
}
