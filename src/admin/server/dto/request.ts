export class CreateServerInput {

    domainName: string;
    ipAddress: string;
}

export class DeleteServerInput {
    serverId: number
}
