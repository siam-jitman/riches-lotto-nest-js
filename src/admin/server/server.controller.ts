import { Body, Controller, Logger, Post } from '@nestjs/common';
import { ServerService } from './server.service';
import { ServerResponseDTO } from './dto/reponse';
import constants from 'src/utils/constants';
import { CreateServerInput, DeleteServerInput } from './dto/request';

@Controller('/admin/server')
export class ServerController {

  private readonly logger = new Logger(ServerController.name);

  constructor(private readonly serverService: ServerService) { }

  @Post('/create')
  async createServer(@Body() request: CreateServerInput): Promise<ServerResponseDTO> {

    let response: ServerResponseDTO = {
      resultType: false,
      resultMessage: ""
    };

    if (!request?.ipAddress || !request?.domainName) {
      response.resultType = false;
      response.resultMessage = constants.MSG_PLS_INPUT_REQUIRE_FIELD
    } else {
      let result = await this.serverService.createServer(request);
      if (result) {
        response.resultType = true;
        response.resultMessage = constants.MSG_CREATE_SUCCESS
      }
    }

    return response;
  }

  @Post('/delete')
  async deleteServer(@Body() request: DeleteServerInput): Promise<ServerResponseDTO> {

    let response: ServerResponseDTO = {
      resultType: false,
      resultMessage: ""
    };

    if (!request?.serverId) {
      response.resultType = false;
      response.resultMessage = constants.MSG_PLS_INPUT_DELETE_SERVER_ID
    } else {
      let result = await this.serverService.deleteServer(request);
      if (result.affected > 0) {
        response.resultType = true;
        response.resultMessage = constants.MSG_DELETE_SUCCESS
      }
    }

    return response;
  }

}
