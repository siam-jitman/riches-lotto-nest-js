import { CommonResponseDTO } from '../../../utils/dto/reponse'

export class AgentListResponseDTO extends CommonResponseDTO {
    resultData?: AgentListDataResponseDTO
}

export class AgentListDataResponseDTO {
    agentList: AgentListItemDataResponseDTO[]
}

export class AgentListItemDataResponseDTO {
    id: string
    value: string
}

export class AgentDetailResponseDTO extends CommonResponseDTO {
    resultData?: AgentDetailDataResponseDTO
}

export class AgentDetailDataResponseDTO {
    agentId: string
    bankOfAgentId: number
    firstname?: string
    lastname?: string
    tel?: string
    email?: string
    bankId?: number
    bankAccountName?: string
    bankAccountId?: string
}
