export class AdminAgentDetailInput {
    agentId?: string
    bankOfAgentId?: number
    firstname: string
    lastname: string
    tel: string
    email: string
    bankId: number
    bankAccountName: string
    bankAccountId: string
    registerDate?: Date
    registerYear?: string
    registerMonth?: string
    status?: string
    type?: string
    password?: string
    serverId?: number
}

