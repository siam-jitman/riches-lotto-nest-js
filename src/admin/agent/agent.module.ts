import { Module } from '@nestjs/common';
import { AgentService } from './agent.service';
import { ServerService } from '../server/server.service';
import { AgentController } from './agent.controller';
// import { AgentResponseDTO, AgentResponseWithoutPagingDTO } from './dto/reponse';
import { t_agent_info } from '../../utils/db/entities/tables/agent/t_agent_info';
import { t_bank_of_agent } from '../../utils/db/entities/tables/agent/t_bank_of_agent';
import { m_server_info } from '../../utils/db/entities/tables/master/m_server_info';
import { t_server_agent_info } from '../../utils/db/entities/tables/agent/t_server_agent_info';
import { t_agent_pass_info } from '../../utils/db/entities/tables/agent/t_agent_pass_info';
import { TypeOrmModule } from '@nestjs/typeorm';
import moment from 'moment';

@Module({
  imports: [
    TypeOrmModule.forFeature([t_agent_info, t_bank_of_agent, m_server_info, t_server_agent_info, t_agent_pass_info]),
  ],
  providers: [{
    provide: 'MomentWrapper',
    useValue: moment
  },
    AgentService,
    ServerService],
  controllers: [AgentController]
})
export class AgentModule { }
