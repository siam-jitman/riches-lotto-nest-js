import { Body, Controller, Logger, Post } from '@nestjs/common';
import { AgentService } from './agent.service';
import { ServerService } from '../server/server.service';
import { AgentDetailDataResponseDTO, AgentDetailResponseDTO, AgentListDataResponseDTO, AgentListItemDataResponseDTO, AgentListResponseDTO } from './dto/reponse';
import constants from 'src/utils/constants';
import { AdminAgentDetailInput } from './dto/request';
import * as moment from 'moment';
import * as randomstring from 'randomstring';
import Utils from 'src/utils/utils';

@Controller('/admin/agent')
export class AgentController {

  private readonly logger = new Logger(AgentController.name);
  private utils = new Utils();

  constructor(
    private readonly agentService: AgentService,
    private readonly serverService: ServerService,
  ) {

  }

  @Post('/create')
  async createAgentDetail(@Body() request: AdminAgentDetailInput): Promise<AgentDetailResponseDTO> {
    let response: AgentDetailResponseDTO = {
      resultType: false,
      resultMessage: ""
    };

    if (!request.bankAccountId || !request.bankAccountName || !request.bankId || !request.email || !request.firstname || !request.lastname || !request.tel) {
      response.resultType = false;
      response.resultMessage = constants.MSG_PLS_INPUT_REQUIRE_FIELD
    } else {

      let serverData = await this.serverService.getServerAvaliableOne();
      if (!serverData) {
        response.resultType = false;
        response.resultMessage = constants.MSG_SERVER_NOT_AVALIABLE;
      } else {
        await this.serverService.updateServerAvaliable(serverData.server_id, false);
        try {

          //Call API : ?Register? ของระบบฝาก-ถอนออโต้
          // ถ้า Call API : ?Register? ของระบบฝาก-ถอนออโต้ ไม่สำเร็จ --> await this.serverService.updateServerAvaliable(serverData.server_id, true);

          let date = new Date();
          let currentYear = moment(date).format('YYYY');
          let currentMonth = moment(date).format('MM');
          let currentUnitAgent = (await this.agentService.countYearAgentInfo(currentYear, currentMonth) + 1).toString().padStart(2, "0");
          let agentId = `AG${currentYear}${currentMonth}${currentUnitAgent}`;

          let passwordAgent = await this.utils.base64Encode(randomstring.generate({
            length: 6,
            charset: '0123456789'
          }));

          let result = await this.agentService.createAgentDetail({
            ...request,
            agentId: agentId,
            registerDate: date,
            registerYear: currentYear,
            registerMonth: currentMonth,
            status: 'A',
            type: 'A',
            password: passwordAgent,
            serverId: serverData.server_id
          });

          if (result) {
            response.resultType = true;
            response.resultMessage = constants.MSG_UPDATE_AGENT_SUCCESS
          } else {
            await this.serverService.updateServerAvaliable(serverData.server_id, true);
          }
        } catch (error) {
          this.logger.error(error);
          await this.serverService.updateServerAvaliable(serverData.server_id, true);
        }
      }
    }

    return response;
  }

  @Post('/update')
  async updateAgentDetail(@Body() request: AdminAgentDetailInput): Promise<AgentDetailResponseDTO> {
    let response: AgentDetailResponseDTO = {
      resultType: false,
      resultMessage: ""
    };

    if (!request.agentId || !request.bankOfAgentId || !request.bankAccountId || !request.bankAccountName || !request.bankId || !request.email || !request.firstname || !request.lastname || !request.tel) {
      response.resultType = false;
      response.resultMessage = constants.MSG_PLS_INPUT_REQUIRE_FIELD
    } else {
      //Call API : ?Update User? ของระบบฝาก-ถอนออโต้
      let result = await this.agentService.updateAgentDetail(request);
      if (result) {
        response.resultType = true;
        response.resultMessage = constants.MSG_UPDATE_AGENT_SUCCESS
      }
    }

    return response;
  }

  @Post('/detail')
  async getAgentDetail(@Body() request: AdminAgentDetailInput): Promise<AgentDetailResponseDTO> {

    let response: AgentDetailResponseDTO = {
      resultType: false,
      resultMessage: ""
    };

    if (!request.agentId) {
      response.resultType = false;
      response.resultMessage = constants.MSG_PLS_INPUT_REQUIRE_FIELD
    } else {
      let result = await this.agentService.getAgentDetail(request);
      if (result) {

        let agentDetailResponse: AgentDetailDataResponseDTO = {
          agentId: result.agent_id,
          firstname: result.firstname,
          lastname: result.lastname,
          email: result.email,
          tel: result.tel,
          bankOfAgentId: result.t_bank_of_agent.bank_of_agent_id,
          bankId: result.t_bank_of_agent.bank_id,
          bankAccountId: result.t_bank_of_agent.bank_account_id,
          bankAccountName: result.t_bank_of_agent.bank_account_name,
        }

        response.resultType = true;
        response.resultMessage = constants.MSG_SEARCH_SUCCESS
        response.resultData = agentDetailResponse;
      }
    }
    return response;
  }

  @Post('/list/dropdown')
  async getAgentList(): Promise<AgentListResponseDTO> {

    let response: AgentListResponseDTO = {
      resultType: false,
      resultMessage: ""
    };
    let agentListItemResponse: AgentListItemDataResponseDTO[] = [];
    let agentListResponse: AgentListDataResponseDTO = {
      agentList: []
    };

    let dataAgentList = await this.agentService.getAgentList();
    dataAgentList.map(async (item) => {
      agentListItemResponse.push({
        id: item.agent_id,
        value: `${item.agent_id} ${item.firstname} ${item.lastname}`
      })
    });

    agentListResponse.agentList = agentListItemResponse;
    response.resultType = true;
    response.resultMessage = constants.MSG_API_SUCCESS;
    response.resultData = agentListResponse;

    return response;
  }

}
