import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { t_agent_info } from '../../utils/db/entities/tables/agent/t_agent_info';
import { t_bank_of_agent } from '../../utils/db/entities/tables/agent/t_bank_of_agent';
import { t_agent_pass_info } from '../../utils/db/entities/tables/agent/t_agent_pass_info'; 
import { t_server_agent_info } from '../../utils/db/entities/tables/agent/t_server_agent_info';  
import { AdminAgentDetailInput } from './dto/request';

@Injectable()
export class AgentService {
  private readonly logger = new Logger(AgentService.name);

  constructor(
    @InjectRepository(t_agent_info)
    private readonly agentInfoRepository: Repository<t_agent_info>,
    @InjectRepository(t_bank_of_agent)
    private readonly bankOfAgentRepository: Repository<t_bank_of_agent>,
    @InjectRepository(t_agent_pass_info)
    private readonly agentPassInfoRepository: Repository<t_agent_pass_info>,
    @InjectRepository(t_server_agent_info)
    private readonly serverAgentInfoRepository: Repository<t_server_agent_info>,
  ) { }

  async getAgentList(): Promise<t_agent_info[]> {

    const findAgent = await this.agentInfoRepository.find({
      where: [{
        status: "A",
      }]
    });

    this.logger.debug('getAgentList --> findAgent --> ', JSON.stringify(findAgent));

    return findAgent;
  }

  async getAgentDetail(req: AdminAgentDetailInput): Promise<t_agent_info> {

    const findAgentDetail = await this.agentInfoRepository.findOne({
      where: [{
        agent_id: req.agentId,
      }],
      relations: ['t_bank_of_agent']
    });

    this.logger.debug('getAgentDetail --> findAgentDetail --> ', JSON.stringify(findAgentDetail));

    return findAgentDetail;
  }

  async createAgentDetail(req: AdminAgentDetailInput): Promise<boolean> {

    try {
      const saveAgentDetail = await this.agentInfoRepository.save({
        agent_id: req.agentId,
        email: req.email,
        firstname: req.firstname,
        lastname: req.lastname,
        tel: req.tel,
        register_date: req.registerDate,
        register_year: req.registerYear,
        register_month: req.registerMonth,
        status: req.status,
        type: req.type,
      });

      const saveAgentPassInfo = await this.agentPassInfoRepository.save({
        agent_id: req.agentId,
        password: req.password
      })

      const saveBankOfAgent = await this.bankOfAgentRepository.save({
        // bank_of_agent_id: req.bankOfAgentId,
        agent_id: req.agentId,
        bank_id: req.bankId,
        bank_account_id: req.bankAccountId,
        bank_account_name: req.bankAccountName,
      })

      const save = await this.serverAgentInfoRepository.save({
        agent_id: req.agentId,
        server_id: req.serverId
      })

      this.logger.debug('updateAgentDetail --> saveAgentDetail --> ', JSON.stringify(saveAgentDetail));
      this.logger.debug('updateAgentDetail --> saveAgentPassInfo --> ', JSON.stringify(saveAgentPassInfo));
      this.logger.debug('updateAgentDetail --> saveBankOfAgent --> ', JSON.stringify(saveBankOfAgent));

      return true;
    } catch (error) {
      return false;
    }
  }

  async updateAgentDetail(req: AdminAgentDetailInput): Promise<boolean> {

    try {
      const saveAgentDetail = await this.agentInfoRepository.save({
        agent_id: req.agentId,
        email: req.email,
        firstname: req.firstname,
        lastname: req.lastname,
        tel: req.tel,
      });

      const saveBankOfAgent = await this.bankOfAgentRepository.save({
        bank_of_agent_id: req.bankOfAgentId,
        agent_id: req.agentId,
        bank_id: req.bankId,
        bank_account_id: req.bankAccountId,
        bank_account_name: req.bankAccountName,
      })

      this.logger.debug('updateAgentDetail --> saveAgentDetail --> ', JSON.stringify(saveAgentDetail));
      this.logger.debug('updateAgentDetail --> saveBankOfAgent --> ', JSON.stringify(saveBankOfAgent));

      return true;
    } catch (error) {
      return false;
    }
  }

  async countYearAgentInfo(year: string, month: string): Promise<number> {

    const count = await this.agentInfoRepository.count({
      where: [{
        register_year:year,
        register_month: month
      }]
    });

    this.logger.debug(`countYearAgentInfo --> count --> year ${year} --> `, JSON.stringify(count));

    return count;
  }
}
