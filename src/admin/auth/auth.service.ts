import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthInput } from './dto/request';
import { t_admin_info } from '../../utils/db/entities/tables/admin/t_admin_info';
import { t_admin_pass_info } from '../../utils/db/entities/tables/admin/t_admin_pass_info';
import { Repository } from 'typeorm';
import { UserInfo, LoginFailType, UserType } from './dto/type';
import { AuthDataResponseDTO, AuthResponseDTO } from './dto/reponse';
import Utils from 'src/utils/utils';
import { t_admin_fail_login } from 'src/utils/db/entities/tables/admin/t_admin_fail_login';
import Constants from 'src/utils/constants';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService extends PassportStrategy(Strategy) {

    private readonly logger = new Logger(AuthService.name);
    private utils = new Utils();

    constructor(
        @InjectRepository(t_admin_info)
        private readonly tAdminInfoRepository: Repository<t_admin_info>,
        @InjectRepository(t_admin_pass_info)
        private readonly tAdminPassInfo: Repository<t_admin_pass_info>,
        @InjectRepository(t_admin_fail_login)
        private readonly tAdminFailLoginRepository: Repository<t_admin_fail_login>,
        private jwtService: JwtService
    ) {
        super({
            secretOrKey: process.env.JWT_SECRET,
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        });
    }

    async getLogin(req: AuthInput): Promise<AuthResponseDTO> {

        let response: AuthResponseDTO = {
            resultType: false,
            resultMessage: ""
        };


        let userInfo: UserInfo = await this.getUserInfoByUsername(req.username);
        this.logger.log('userInfo --> ', JSON.stringify(userInfo, null, 3));
        if (userInfo.data) {
            let status = (userInfo?.data?.status).toString();
            if (status === "A") {
                let adminInfo = userInfo.data as t_admin_info;
                let password = await this.utils.base64Encode(req.password);

                let adminPassInfo = await this.getAdminPassInfoByAdminIdAndPassword(adminInfo.admin_id, password);
                this.logger.log('adminPassInfo --> ', JSON.stringify(adminPassInfo, null, 3));

                if (adminPassInfo) {

                    const login_date = new Date();
                    const payload: object = JSON.parse(JSON.stringify(adminInfo));
                    const accessToken: string = this.jwtService.sign(payload);
                    const resultData: AuthDataResponseDTO = {
                        adminId: adminPassInfo.admin_id,
                        loginTime: login_date,
                        accessToken: accessToken
                    }

                    await this.deleteAdminFailLogin(adminPassInfo.admin_id);
                    await this.updateLastLogin(adminInfo, login_date);

                    response.resultType = true;
                    response.resultMessage = Constants.MSG_LOGIN_SUCCESS;
                    response.resultData = resultData;

                } else {
                    let loginFail = await this.checkLoginFail(adminInfo);
                    this.logger.log('loginFail --> ' + loginFail);
                    if (loginFail === LoginFailType.PASS) {
                        response.resultType = false;
                        response.resultMessage = Constants.MSG_LOGIN_FAIL_WRONG_PASS_WARNING_USER_LOCK;
                    } else {
                        response.resultType = false;
                        response.resultMessage = Constants.MSG_LOGIN_FAILADMIN_LOCK_PLS_SUPER_ADMIN;
                    }
                }
            } else if (status === "I") {
                response.resultType = false;
                response.resultMessage = Constants.MSG_LOGIN_FAILADMIN_LOCK_PLS_SUPER_ADMIN;
            } else if (status === "B") {
                response.resultType = false;
                response.resultMessage = Constants.MSG_LOGIN_FAILADMIN_BLOCK_PLS_SUPER_ADMIN;
            }
        } else {
            response.resultType = false;
            response.resultMessage = Constants.MSG_LOGIN_FAIL_ADMIN_NOT_FOUND;
        }

        this.logger.log('response --> ', JSON.stringify(response, null, 3));
        return response;
    }

    private async deleteAdminFailLogin(admin_id: string): Promise<void> {
        await this.tAdminFailLoginRepository.delete({ admin_id: admin_id })
    }


    private async updateLastLogin(adminInfo: t_admin_info, login_date: Date): Promise<void> {
        await this.tAdminInfoRepository.upsert([{ ...adminInfo, lasted_login_date: login_date }],
            ['admin_id'])
    }

    private async checkLoginFail(adminInfo: t_admin_info): Promise<LoginFailType> {
        let failLoginData: t_admin_fail_login;
        let numberFailLogin: number;

        failLoginData = await this.getAdminFailLoginByAdminId(adminInfo.admin_id);
        numberFailLogin = !failLoginData ? 0 : failLoginData.fail_login_total;

        if (numberFailLogin <= 3) {
            await this.updateAdminFailLogin(adminInfo, numberFailLogin + 1);
            return LoginFailType.PASS;
        } else {
            return LoginFailType.NOT_PASS;
        }
    }

    private async updateAdminFailLogin(adminInfo: t_admin_info, numberFailLogin: number): Promise<void> {
        await this.tAdminFailLoginRepository.upsert([{ ...adminInfo, fail_login_total: numberFailLogin }],
            ['admin_id'])
    }

    private async getAdminFailLoginByAdminId(admin_id: string): Promise<t_admin_fail_login> {
        let adminFailLogin: t_admin_fail_login;
        adminFailLogin = await this.tAdminFailLoginRepository.findOne({
            where: [{
                admin_id: admin_id,
            }]
        })
        return adminFailLogin;
    }

    private async getAdminPassInfoByAdminIdAndPassword(admin_id: string, password: string): Promise<t_admin_pass_info> {
        let adminPassInfo: t_admin_pass_info;
        adminPassInfo = await this.tAdminPassInfo.findOne({
            where: [{
                admin_id: admin_id,
                password: password
            }]
        })
        return adminPassInfo;
    }

    private async getUserInfoByUsername(username: string): Promise<UserInfo> {
        let userInfo: UserInfo = {};
        let adminInfo: t_admin_info;

        adminInfo = await this.tAdminInfoRepository.findOne({
            where: [{
                admin_id: username,
            }]
        })

        userInfo = {
            type: UserType.ADMIN,
            data: adminInfo
        }

        return userInfo
    }

}
