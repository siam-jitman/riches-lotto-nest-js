import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { entities } from '../../utils/db/entities';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

const jwtFactory = {
  useFactory: async () => ({
    secret: process.env.JWT_SECRET,
    signOptions: {
      expiresIn: process.env.JWT_EXP_H,
    },
  }),
};

@Module({
  imports: [
    TypeOrmModule.forFeature(entities),
    JwtModule.registerAsync(jwtFactory),
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],

  controllers: [AuthController],
  providers: [AuthService, JwtModule, PassportModule]
})
export class AuthModule { }
