import { CommonResponseDTO } from '../../../utils/dto/reponse'

export class AuthResponseDTO extends CommonResponseDTO {
    resultData?: AuthDataResponseDTO
}

export class AuthDataResponseDTO {
    adminId: string
    loginTime: Date
    accessToken?: string
}

