
import { t_admin_info } from '../../../utils/db/entities/tables/admin/t_admin_info';

export enum LoginFailType {
    PASS,
    NOT_PASS,
}

export enum UserType {
    ADMIN,
}
export class UserInfo {
    type?: UserType;
    data?: t_admin_info
}
