import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { m_all_status } from '../../utils/db/entities/tables/master/m_all_status'

@Injectable()
export class StatusService {
  private readonly logger = new Logger(StatusService.name);

  constructor(
    @InjectRepository(m_all_status)
    private readonly StatusRepository: Repository<m_all_status>,
) { }

  async getStatus(): Promise<m_all_status[]> {

    const findStatus = await this.StatusRepository.find();

  this.logger.debug('findStatus --> ', JSON.stringify(findStatus));

    return findStatus;
  }
}
