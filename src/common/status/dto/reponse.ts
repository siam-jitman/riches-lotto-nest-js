import { CommonResponseDTO } from '../../../utils/dto/reponse'

export class StatusListResponseDTO extends CommonResponseDTO {
    resultData?: StatusListDataResponseDTO
}

export class StatusListDataResponseDTO {
    statusList: StatusListItemDataResponseDTO[]
}

export class StatusListItemDataResponseDTO {
    id: string
    value: string
}

