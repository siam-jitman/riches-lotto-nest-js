import { Module } from '@nestjs/common';
import { StatusService } from './status.service';
import { StatusController } from './status.controller';
// import { StatusResponseDTO, StatusResponseWithoutPagingDTO } from './dto/reponse';
import { m_all_status } from '../../utils/db/entities/tables/master/m_all_status';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([m_all_status]),
  ],
  providers: [StatusService],
  controllers: [StatusController]
})
export class StatusModule { }
