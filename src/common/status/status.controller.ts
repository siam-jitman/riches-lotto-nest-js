import { Controller, Logger, Post } from '@nestjs/common';
import { StatusService } from './status.service';
import { StatusListDataResponseDTO, StatusListItemDataResponseDTO, StatusListResponseDTO } from './dto/reponse';
import constants from 'src/utils/constants';

@Controller('/common/status')
export class StatusController {

  private readonly logger = new Logger(StatusController.name);

  constructor(private readonly statusService: StatusService) { }

  @Post('/list')
  async getStatus(): Promise<StatusListResponseDTO> {

    let response: StatusListResponseDTO = {
      resultType: false,
      resultMessage: ""
    };
    let statusListItemResponse: StatusListItemDataResponseDTO[] = [];
    let statusListResponse: StatusListDataResponseDTO = {
      statusList: []
    };

    let dataStatusList = await this.statusService.getStatus();
    dataStatusList.map(async (item) => {
      statusListItemResponse.push({
        id: item.status_id,
        value: item.status_th
      })
    });

    statusListResponse.statusList = statusListItemResponse;
    response.resultType = true;
    response.resultMessage = constants.MSG_GET_STATUS_LIST_SUCCESS;
    response.resultData = statusListResponse;

    return response;
  }

}
