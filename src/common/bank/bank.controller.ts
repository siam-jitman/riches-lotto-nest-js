import { Controller, Logger, Post } from '@nestjs/common';
import { BankService } from './bank.service';
import { BankListDataResponseDTO, BankListItemDataResponseDTO, BankListResponseDTO } from './dto/reponse';
import constants from 'src/utils/constants';

@Controller('/common/bank')
export class BankController {

  private readonly logger = new Logger(BankController.name);

  constructor(private readonly bankService: BankService) { }

  @Post('/list')
  async getBank(): Promise<BankListResponseDTO> {

    let response: BankListResponseDTO = {
      resultType: false,
      resultMessage: ""
    };
    let bankListItemResponse: BankListItemDataResponseDTO[] = [];
    let bankListResponse: BankListDataResponseDTO = {
      bankList: []
    };

    let dataBankList = await this.bankService.getBank();
    dataBankList.map(async (item) => {
      bankListItemResponse.push({
        id: item.bank_id,
        value: item.bank_name_th
      })
    });

    bankListResponse.bankList = bankListItemResponse;
    response.resultType = true;
    response.resultMessage = constants.MSG_GET_BANK_LIST_SUCCESS;
    response.resultData = bankListResponse;

    return response;
  }

}
