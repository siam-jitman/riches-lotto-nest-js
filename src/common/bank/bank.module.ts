import { Module } from '@nestjs/common';
import { BankService } from './bank.service';
import { BankController } from './bank.controller';
// import { BankResponseDTO, BankResponseWithoutPagingDTO } from './dto/reponse';
import { m_bank_info } from '../../utils/db/entities/tables/master/m_bank_info';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
      TypeOrmModule.forFeature([ m_bank_info]),
  ],
  providers: [BankService],
  controllers: [BankController]
})
export class BankModule {}
