import { CommonResponseDTO } from '../../../utils/dto/reponse'

export class BankListResponseDTO extends CommonResponseDTO {
    resultData?: BankListDataResponseDTO
}

export class BankListDataResponseDTO {
    bankList: BankListItemDataResponseDTO[]
}

export class BankListItemDataResponseDTO {
    id: number
    value: string
}

