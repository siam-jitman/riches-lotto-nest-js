import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { m_bank_info } from '../../utils/db/entities/tables/master/m_bank_info'

@Injectable()
export class BankService {
  private readonly logger = new Logger(BankService.name);

  constructor(
    @InjectRepository(m_bank_info)
    private readonly BankRepository: Repository<m_bank_info>,
) { }

  async getBank(): Promise<m_bank_info[]> {

    const findBank = await this.BankRepository.find({
      where: [{
          bank_sts: "A",
      }]
  });

  this.logger.debug('findBank --> ', JSON.stringify(findBank));

    return findBank;
  }
}
