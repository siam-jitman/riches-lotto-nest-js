import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { m_posts } from '../../utils/db/entities/tables/m_posts'

@Injectable()
export class PostsService {

  constructor(
    @InjectRepository(m_posts)
    private readonly postsRepository: Repository<m_posts>,
) { }

  async getPosts(): Promise<m_posts[]> {

    const fResult = await this.postsRepository.find();

    return fResult;
  }
}
