import { m_posts } from '../../../utils/db/entities/tables/m_posts'
import { PaginationResponseDTO } from '../../../utils/dto/reponse'

export class PostsResponseDTO extends PaginationResponseDTO {
    dataList: PostsResponseWithoutPagingDTO[]
}

export class PostsResponseWithoutPagingDTO extends m_posts {
    date: Date
}