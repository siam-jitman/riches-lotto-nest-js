import { Controller, Get, Post } from '@nestjs/common';
import { PostsService } from './posts.service';
import { m_posts } from '../../utils/db/entities/tables/m_posts';

@Controller('/posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post('/getPosts')
  async getPosts(): Promise<m_posts[]> {
    return await this.postsService.getPosts();
  }

}
