import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
// import { PostsResponseDTO, PostsResponseWithoutPagingDTO } from './dto/reponse';
import { m_posts } from '../../utils/db/entities/tables/m_posts';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
      TypeOrmModule.forFeature([ m_posts]),
  ],
  providers: [PostsService],
  controllers: [PostsController]
})
export class PostsModule {}
