import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_agent_info } from './t_agent_info'


@Entity({ name: 't_agent_pass_info' })
export class t_agent_pass_info {

    @PrimaryGeneratedColumn({ name: 'agent_pass_id' })
    agent_pass_id: string;

    @ManyToOne(() => t_agent_info, (tAgentInfo) => tAgentInfo.agent_id, { cascade: true })
    @JoinColumn({ name: 'agent_id', foreignKeyConstraintName: 'FK-t_agent_pass_info.agent_id-t_agent_info.agent_id' })
    @Column({ nullable: false })
    agent_id: string

    @Column({ name: 'password', type: 'varchar', length: 150 })
    password: string;

}