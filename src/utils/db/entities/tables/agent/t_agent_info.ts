import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, OneToOne } from 'typeorm';
import { m_all_status } from '../master/m_all_status'
import { t_bank_of_agent } from './t_bank_of_agent';


@Entity({ name: 't_agent_info' })
export class t_agent_info {

    // @Column({length: 10})
    @PrimaryColumn({ name: 'agent_id', type: 'varchar' })
    agent_id: string;

    @Column({ length: 30, type: 'varchar' })
    firstname: string;

    @Column({ length: 50, type: 'varchar' })
    lastname: string;

    @Column({ length: 10, type: 'varchar' })
    tel: string;

    @Column({ length: 100, type: 'varchar', unique: true })
    email: string;

    @Column({ name: 'register_date', type: 'timestamptz' })
    register_date: Date

    // length: 4
    @Column({ name: 'register_year', type: 'varchar' , length: 4})
    register_year: string

    // length: 2
    @Column({ name: 'register_month', type: 'varchar' , length: 2})
    register_month: string

    @ManyToOne(() => m_all_status, (mAllStatus) => mAllStatus.status_id, { cascade: true })
    @JoinColumn({ name: 'status', foreignKeyConstraintName: 'FK-t_agent_info.status-m_all_status.status_id' })
    @Column({ nullable: false })
    status: string

    @OneToOne(() => t_bank_of_agent, (tBankOfAgent) => tBankOfAgent.agent_id, { cascade: false })
    t_bank_of_agent?: t_bank_of_agent

    @Column({ length: 1, type: 'varchar' })
    type: string;

}