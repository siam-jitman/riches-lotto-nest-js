import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_agent_info } from './t_agent_info'
import { m_server_info } from '../master/m_server_info'


@Entity({ name: 't_server_agent_info' })
export class t_server_agent_info {

    @PrimaryGeneratedColumn({ name: 'server_agent_id' })
    server_agent_id: string;

    @ManyToOne(() => t_agent_info, (tAgentInfo) => tAgentInfo.agent_id, { cascade: true })
    @JoinColumn({ name: 'agent_id', foreignKeyConstraintName: 'FK-t_server_agent_info.agent_id-t_agent_info.agent_id' })
    @Column({ nullable: false })
    agent_id: string

    @ManyToOne(() => m_server_info, (mServerInfo) => mServerInfo.server_id, { cascade: true })
    @JoinColumn({ name: 'server_id', foreignKeyConstraintName: 'FK-t_server_agent_info.server_id-m_server_info.server_id' })
    @Column({ nullable: false })
    server_id: number

}