import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_agent_info } from './t_agent_info'


@Entity({ name: 't_agent_fail_login' })
export class t_agent_fail_login {

    @PrimaryGeneratedColumn({ name: 'fail_login_id' })
    fail_login_id: string;

    @ManyToOne(() => t_agent_info, (tAgentInfo) => tAgentInfo.agent_id, { cascade: true })
    @JoinColumn({ name: 'agent_id', foreignKeyConstraintName: 'FK-t_agent_fail_login.agent_id-t_agent_info.agent_id' })
    @Column({ nullable: false })
    agent_id: t_agent_info

    @Column({ name: 'fail_login_total', type: 'int' })
    fail_login_total: string;

}