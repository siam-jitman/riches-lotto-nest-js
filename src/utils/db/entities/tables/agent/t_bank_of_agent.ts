import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_agent_info } from './t_agent_info'
import { m_bank_info } from '../master/m_bank_info'


@Entity({ name: 't_bank_of_agent' })
export class t_bank_of_agent {

    @PrimaryGeneratedColumn({ name: 'bank_of_agent_id' })
    bank_of_agent_id: number;

    @ManyToOne(() => t_agent_info, (tAgentInfo) => tAgentInfo.agent_id, { cascade: true })
    @JoinColumn({ name: 'agent_id', foreignKeyConstraintName: 'FK-t_bank_of_agent.agent_id-t_agent_info.agent_id' })
    @Column({ nullable: false, unique: true })
    agent_id: string

    @ManyToOne(() => m_bank_info, (mBankInfo) => mBankInfo.bank_id, { cascade: true })
    @JoinColumn({ name: 'bank_id', foreignKeyConstraintName: 'FK-t_bank_of_agent.bank_id-m_bank_info.bank_id' })
    @Column({ nullable: false })
    bank_id: number

    @Column({ length: 150, type: 'varchar' })
    bank_account_name: string;

    @Column({ length: 15, type: 'varchar' })
    bank_account_id: string;

}