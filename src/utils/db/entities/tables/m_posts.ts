import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity({ name: 'm_posts' })
export class m_posts {

    @PrimaryGeneratedColumn({ name: 'posts_id', type: 'int' })
    posts_id: number;

    @Column({ name: 'title', type: 'varchar', length: 255 })
    title: string;

    @Column({ name: 'cover_img', type: 'varchar', length: 255 })
    cover_img: string;

    @Column({ name: 'link_img', type: 'varchar', length: 255 })
    link_img: string;

}