import { Entity, PrimaryColumn, Column } from 'typeorm';


@Entity({ name: 'm_customer_type' })
export class m_customer_type {

    // @Column({length: 1})
    @PrimaryColumn({ name: 'customer_type_id', type: 'varchar' })
    customer_type_id: string;

    @Column({ name: 'type_th', type: 'varchar', length: 15 })
    type_th: string;

    @Column({ name: 'type_en', type: 'varchar', length: 10 })
    type_en: string;

}