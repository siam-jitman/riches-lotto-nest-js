import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_customer_info } from './t_customer_info'


@Entity({ name: 't_customer_pass_info' })
export class t_customer_pass_info {

    @PrimaryGeneratedColumn({ name: 'customer_pass_id' })
    customer_pass_id: string;

    @ManyToOne(() => t_customer_info, (tCustomerInfo) => tCustomerInfo.customer_id, { cascade: true })
    @JoinColumn({ name: 'customer_id', foreignKeyConstraintName: 'FK-t_customer_pass_info.customer_id-t_customer_info.customer_id' })
    @Column({ nullable: false })
    customer_id: number

    @Column({ name: 'password', type: 'varchar', length: 150 })
    password: string;

}