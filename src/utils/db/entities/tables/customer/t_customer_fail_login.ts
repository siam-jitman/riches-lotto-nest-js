import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_customer_info } from './t_customer_info'


@Entity({ name: 't_customer_fail_login' })
export class t_customer_fail_login {

    @PrimaryGeneratedColumn({ name: 'fail_login_id' })
    fail_login_id: string;

    @ManyToOne(() => t_customer_info, (tCustomerInfo) => tCustomerInfo.customer_id, { cascade: true })
    @JoinColumn({ name: 'customer_id', foreignKeyConstraintName: 'FK-t_customer_fail_login.customer_id-t_customer_info.customer_id' })
    @Column({ nullable: false, unique: true })
    customer_id: number

    @Column({ name: 'fail_login_total', type: 'int' })
    fail_login_total: number;

}