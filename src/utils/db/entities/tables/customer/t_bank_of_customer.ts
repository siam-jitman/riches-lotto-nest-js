import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_customer_info } from './t_customer_info'
import { m_bank_info } from '../master/m_bank_info'


@Entity({ name: 't_bank_of_customer' })
export class t_bank_of_customer {

    @PrimaryGeneratedColumn({ name: 'bank_of_customer_id' })
    bank_of_customer_id: string;

    @ManyToOne(() => t_customer_info, (tCustomerInfo) => tCustomerInfo.customer_id, { cascade: true })
    @JoinColumn({ name: 'customer_id', foreignKeyConstraintName: 'FK-t_bank_of_customer.customer_id-t_customer_info.customer_id' })
    @Column({ nullable: false })
    customer_id: t_customer_info

    @ManyToOne(() => m_bank_info, (mBankInfo) => mBankInfo.bank_id, { cascade: true })
    @JoinColumn({ name: 'bank_id', foreignKeyConstraintName: 'FK-t_bank_of_customer.bank_id-m_bank_info.bank_id' })
    @Column({ nullable: false })
    bank_id: m_bank_info

    @Column({ length: 150, type: 'varchar' })
    bank_account_name: string;

    @Column({ length: 15, type: 'varchar' })
    bank_account_id: string;

}