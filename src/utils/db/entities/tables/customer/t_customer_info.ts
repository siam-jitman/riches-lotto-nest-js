import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { m_all_status } from '../master/m_all_status'
import { m_customer_type } from './m_customer_type'
import { t_agent_info } from '../agent/t_agent_info'


@Entity({ name: 't_customer_info' })
export class t_customer_info {

    @PrimaryGeneratedColumn({ name: 'customer_id', type: 'int' })
    customer_id: number;

    @Column({ length: 10, type: 'varchar', unique: true })
    tel: string;

    @Column({ name: 'register_date', type: 'timestamptz' })
    register_date: Date

    @ManyToOne(() => m_all_status, (mAllStatus) => mAllStatus.status_id, { cascade: true })
    @JoinColumn({ name: 'status', foreignKeyConstraintName: 'FK-t_customer_info.status-m_all_status.customer_sts_id' })
    @Column({ nullable: false })
    status: m_all_status

    @ManyToOne(() => m_customer_type, (mCustomerType) => mCustomerType.customer_type_id, { cascade: true })
    @JoinColumn({ name: 'type', foreignKeyConstraintName: 'FK-t_customer_info.type-m_customer_type.customer_type_id' })
    @Column({ nullable: false })
    type: m_customer_type

    @ManyToOne(() => t_agent_info, (tAgentInfo) => tAgentInfo.agent_id, { cascade: true })
    @JoinColumn({ name: 'agent_id', foreignKeyConstraintName: 'FK-t_customer_info.agent_id-t_agent_info.agent_id' })
    @Column({ nullable: false })
    agent_id: t_agent_info

    @Column({ name: 'upline', length: 10, type: 'varchar' })
    upline: string

}