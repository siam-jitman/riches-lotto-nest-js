import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_admin_info } from './t_admin_info'


@Entity({ name: 't_admin_pass_info' })
export class t_admin_pass_info {

    @PrimaryGeneratedColumn({ name: 'admin_pass_id' })
    admin_pass_id: string;

    @ManyToOne(() => t_admin_info, (tAdminInfo) => tAdminInfo.admin_id, { cascade: true })
    @JoinColumn({ name: 'admin_id', foreignKeyConstraintName: 'FK-t_admin_pass_info.admin_id-t_admin_info.admin_id' })
    @Column({ nullable: false })
    admin_id: string

    @Column({ name: 'password', type: 'varchar', length: 150 })
    password: string;

}