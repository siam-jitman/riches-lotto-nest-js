import { Entity, PrimaryColumn, Column } from 'typeorm';


@Entity({ name: 'm_admin_type' })
export class m_admin_type {

    // @Column({length: 1})
    @PrimaryColumn({ name: 'admin_type_id', type: 'varchar'})
    admin_type_id  : string;

    @Column({ name: 'type_th', type: 'varchar', length: 15 })
    type_th: string;

    @Column({ name: 'type_en', type: 'varchar', length: 15 })
    type_en: string;

}