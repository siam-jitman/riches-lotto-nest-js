import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { t_admin_info } from './t_admin_info'


@Entity({ name: 't_admin_fail_login' })
export class t_admin_fail_login {

    @PrimaryGeneratedColumn({ name: 'fail_login_id' })
    fail_login_id: string;

    @ManyToOne(() => t_admin_info, (tAdminInfo) => tAdminInfo.admin_id, { cascade: true })
    @JoinColumn({ name: 'admin_id', foreignKeyConstraintName: 'FK-t_admin_fail_login.admin_id-t_admin_info.admin_id' })
    @Column({ nullable: false })
    admin_id: string

    @Column({ name: 'fail_login_total', type: 'int' })
    fail_login_total: number;

}