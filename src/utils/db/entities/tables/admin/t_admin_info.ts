import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { m_all_status } from '../master/m_all_status'
import { m_admin_type } from './m_admin_type'


@Entity({ name: 't_admin_info' })
export class t_admin_info {

    // @Column({length: 6})
    @PrimaryColumn({ name: 'admin_id', type: 'varchar' })
    admin_id: string;

    @Column({ name: 'firstname', type: 'varchar', length: 30 })
    firstname: string;

    @Column({ name: 'lastname', type: 'varchar', length: 50 })
    lastname: string;

    @Column({ name: 'tel', type: 'varchar', length: 10 })
    tel: string;

    @Column({ name: 'email', type: 'varchar', length: 100, unique: true })
    email: string;

    @Column({ name: 'register_date', type: 'timestamptz' })
    register_date: Date

    @ManyToOne(() => m_all_status, (mAllStatus) => mAllStatus.status_id, { cascade: true })
    @JoinColumn({ name: 'status', foreignKeyConstraintName: 'FK-t_admin_info.status-m_all_status.status_id' })
    @Column({ nullable: false })
    status: m_all_status

    @ManyToOne(() => m_admin_type, (mAdminType) => mAdminType.admin_type_id, { cascade: true })
    @JoinColumn({ name: 'type', foreignKeyConstraintName: 'FK-t_admin_info.type-m_admin_type.admin_type_id' })
    @Column({ nullable: false })
    type: m_admin_type

    @Column({ name: 'lasted_login_date', type: 'timestamptz', nullable: true })
    lasted_login_date: Date

}