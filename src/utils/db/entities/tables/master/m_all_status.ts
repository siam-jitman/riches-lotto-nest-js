import { Entity, PrimaryColumn, Column } from 'typeorm';


@Entity({ name: 'm_all_status' })
export class m_all_status {

    // @Column({length: 1})
    @PrimaryColumn({ name: 'status_id', type: 'varchar', unique: true})
    status_id : string;

    @Column({ name: 'status_th', type: 'varchar', length: 15 })
    status_th: string;

    @Column({ name: 'status_en', type: 'varchar', length: 10 })
    status_en: string;

}