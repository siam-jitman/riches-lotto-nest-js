import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { m_all_status } from './m_all_status'


@Entity({ name: 'm_bank_info' })
export class m_bank_info {

    // @Column({length: 3})
    @PrimaryGeneratedColumn({ name: 'bank_id', type: 'int'})
    bank_id : number;

    @Column({ name: 'bank_name_th', type: 'varchar', length: 50 })
    bank_name_th: string;

    @Column({ name: 'bank_name_en', type: 'varchar', length: 60 })
    bank_name_en: string;

    @Column({ name: 'short_name', type: 'varchar', length: 20 })
    short_name : string;

    @Column({ name: 'switchcode', type: 'varchar', length: 20 })
    switchcode : string;

    @ManyToOne(() => m_all_status, (mAllStatus) => mAllStatus.status_id, { cascade: true })
    @JoinColumn({ name: 'bank_sts', foreignKeyConstraintName: 'FK-m_bank_info.bank_sts-m_all_status.status_id' })
    @Column({ nullable: false })
    bank_sts: string

}