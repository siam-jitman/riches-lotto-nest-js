import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity({ name: 'm_war_info' })
export class m_war_info {

    @PrimaryGeneratedColumn({ name: 'war_id'})
    war_id : string;

    @Column({ name: 'war_name', type: 'varchar', length: 20 })
    war_name: string;

}