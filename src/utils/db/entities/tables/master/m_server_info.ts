import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity({ name: 'm_server_info' })
export class m_server_info {

    @PrimaryGeneratedColumn({ name: 'server_id'})
    server_id : number;

    @Column({ name: 'server_ip', type: 'varchar', length: 15 })
    server_ip: string;

    @Column({ name: 'server_dns', type: 'varchar', length: 100 })
    server_dns: string;

    @Column({ name: 'avaliable', type: 'boolean' })
    avaliable : boolean;

}