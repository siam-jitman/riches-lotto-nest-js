import { m_all_status } from './tables/master/m_all_status'
import { m_bank_info } from './tables/master/m_bank_info'
import { m_server_info } from './tables/master/m_server_info'
import { m_war_info } from './tables/master/m_war_info'

import { m_admin_type } from './tables/admin/m_admin_type'
import { t_admin_fail_login } from './tables/admin/t_admin_fail_login'
import { t_admin_info } from './tables/admin/t_admin_info'
import { t_admin_pass_info } from './tables/admin/t_admin_pass_info'

import { t_agent_fail_login } from './tables/agent/t_agent_fail_login'
import { t_agent_info } from './tables/agent/t_agent_info'
import { t_agent_pass_info } from './tables/agent/t_agent_pass_info'
import { t_bank_of_agent } from './tables/agent/t_bank_of_agent'
import { t_server_agent_info } from './tables/agent/t_server_agent_info'

import { m_customer_type } from './tables/customer/m_customer_type'
import { t_bank_of_customer } from './tables/customer/t_bank_of_customer'
import { t_customer_fail_login } from './tables/customer/t_customer_fail_login'
import { t_customer_info } from './tables/customer/t_customer_info'
import { t_customer_pass_info } from './tables/customer/t_customer_pass_info'

import { m_posts } from './tables/m_posts'

export const entities = [

    m_all_status,
    m_bank_info,
    m_server_info,
    m_war_info,
    m_admin_type,

    t_admin_fail_login,
    t_admin_info,
    t_admin_pass_info,

    t_agent_fail_login,
    t_agent_info,
    t_agent_pass_info,
    t_bank_of_agent,
    t_server_agent_info,

    m_customer_type,
    t_bank_of_customer,
    t_customer_fail_login,
    t_customer_info,
    t_customer_pass_info,

    m_posts,

]