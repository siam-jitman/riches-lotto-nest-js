import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import {entities} from './entities';

export class TypeOrmConfigService {
    public static getTypeOrmOptions(): TypeOrmModuleOptions {
        
        const host = process.env.DB_HOST;
        const username = process.env.DB_USERNAME;
        const password = process.env.DB_PASSWORD;
        const database = process.env.DB_NAME;

        return {
            type: 'postgres',
            host,
            username,
            password,
            database,
            synchronize: true,
            logging: true,
            keepConnectionAlive: true,
            autoLoadEntities: true,
            entities: entities,
            subscribers: [],
            migrations: [],
        };
    }
}