

export class PaginationResponseDTO {
    page: number;
    perPage: number;
    totalPage: number;
    totalRecord: number;
}

export class CommonResponseDTO {
    resultType: boolean;
    resultMessage: string;
    resultData?: any;
    paging?: PaginationResponseDTO;
}