export default class Utils {

    public async base64Encode(text: string): Promise<string> {
        return Buffer.from(text).toString('base64')
    }

    public async base64Decode(code: string): Promise<string> {
        return Buffer.from(code, 'base64').toString('ascii')
    }

}