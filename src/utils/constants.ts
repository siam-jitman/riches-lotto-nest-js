const MSG_LOGIN_FAIL_NOT_USER_PLS_REGISTER = "ไม่พบข้อมูลชื่อผู้ใช้งานในระบบ, กรุณาลงทะเบียน";
const MSG_LOGIN_FAIL_USER_LOCK_PLS_CONTACT_AGENT = "ชื่อผู้ใช้งานของคุณถูกปิดการใช้งาน, กรุณาติดต่อตัวแทนของคุณ";
const MSG_LOGIN_FAIL_WRONG_PASS_WARNING_USER_LOCK = "ข้อมูลชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง, หากระบุไม่ถูกต้องครบ 3 ครั้ง ชื่อผู้ใช้งานของท่านจะถูกห้ามเข้าสู่ระบบ";
const MSG_LOGIN_FAIL_ADMIN_NOT_FOUND = "ไม่พบข้อมูลชื่อผู้ใช้งานในระบบ, กรุณาติดต่อ Super Admin";
const MSG_LOGIN_FAILADMIN_LOCK_PLS_SUPER_ADMIN = "ชื่อผู้ใช้งานของคุณถูกปิดการใช้งาน, กรุณาติดต่อ Super Admin";
const MSG_LOGIN_FAILADMIN_BLOCK_PLS_SUPER_ADMIN = "ชื่อผู้ใช้งานของคุณถูกห้ามเข้าใช้งาน, กรุณาติดต่อ Super Admin";
const MSG_LOGIN_FAIL_NOT_ENTER_USER_OR_PASS = "ต้องระบุ 'ชื่อผู้ใช้งาน' และ 'รหัสผ่าน' ให้ครบถ้วน";
const MSG_LOGIN_SUCCESS = "เข้าสู่ระบบสำเร็จ";
const MSG_GET_BANK_LIST_SUCCESS = "ดึงข้อมูลรายชื่อธนาคารสำเร็จ";
const MSG_GET_STATUS_LIST_SUCCESS = "ดึงข้อมูลสถานะสำเร็จ";
const MSG_CREATE_SUCCESS = "สร้างรายการข้อมูลสำเร็จ";
const MSG_DELETE_SUCCESS = "ลบรายการข้อมูลสำเร็จ";
const MSG_PLS_INPUT_REQUIRE_FIELD = "กรุณาระบุข้อมูลให้ครบถ้วน";
const MSG_PLS_INPUT_DELETE_SERVER_ID = "ต้องระบุ 'Server' ที่ต้องการลบ";
const MSG_API_SUCCESS = "API ทำงานสำเร็จ"; 
const MSG_SEARCH_SUCCESS = "ค้นหาข้อมูลสำเร็จ";
const MSG_UPDATE_AGENT_SUCCESS = "บันทึกการเปลี่ยนแปลงข้อมูล Agent สำเร็จ";
const MSG_SERVER_NOT_AVALIABLE = "ไม่มี Server ว่าง, กรุณาติดต่อ Dev Support";

export default {
    MSG_LOGIN_FAIL_NOT_USER_PLS_REGISTER,
    MSG_LOGIN_FAIL_USER_LOCK_PLS_CONTACT_AGENT,
    MSG_LOGIN_FAIL_WRONG_PASS_WARNING_USER_LOCK,
    MSG_LOGIN_FAIL_ADMIN_NOT_FOUND,
    MSG_LOGIN_FAILADMIN_LOCK_PLS_SUPER_ADMIN,
    MSG_LOGIN_FAILADMIN_BLOCK_PLS_SUPER_ADMIN,
    MSG_LOGIN_FAIL_NOT_ENTER_USER_OR_PASS,
    MSG_LOGIN_SUCCESS,
    MSG_GET_BANK_LIST_SUCCESS,
    MSG_GET_STATUS_LIST_SUCCESS,
    MSG_CREATE_SUCCESS,
    MSG_DELETE_SUCCESS,
    MSG_PLS_INPUT_REQUIRE_FIELD,
    MSG_PLS_INPUT_DELETE_SERVER_ID,
    MSG_API_SUCCESS,
    MSG_SEARCH_SUCCESS,
    MSG_UPDATE_AGENT_SUCCESS,
    MSG_SERVER_NOT_AVALIABLE
}