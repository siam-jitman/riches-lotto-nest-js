import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { entities } from '../../utils/db/entities';

@Module({
  imports: [
      TypeOrmModule.forFeature(entities),
  ],
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule {}
