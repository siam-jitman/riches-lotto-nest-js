import { m_posts } from '../../../utils/db/entities/tables/m_posts'
import { CommonResponseDTO } from '../../../utils/dto/reponse'

export class AuthResponseDTO extends CommonResponseDTO {
    resultData?: AuthDataResponseDTO
}

export class AuthDataResponseDTO {
    id: string
    username: string
    status: string
    role: string
    upline: string
    accessToken: string
}

