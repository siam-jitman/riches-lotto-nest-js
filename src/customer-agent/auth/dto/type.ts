
import { t_customer_info } from '../../../utils/db/entities/tables/customer/t_customer_info';
import { t_agent_info } from '../../../utils/db/entities/tables/agent/t_agent_info';
import { t_customer_pass_info } from 'src/utils/db/entities/tables/customer/t_customer_pass_info';
import { t_agent_pass_info } from 'src/utils/db/entities/tables/agent/t_agent_pass_info';

export enum LoginFailType {
    PASS,
    NOT_PASS,
}

export enum UserType {
    CUSTOMER,
    AGENT,
}
export class UserInfo {
    type?: UserType;
    data?: t_customer_info | t_agent_info
}
