import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthInput } from './dto/request';
import { t_customer_info } from '../../utils/db/entities/tables/customer/t_customer_info';
import { t_customer_pass_info } from '../../utils/db/entities/tables/customer/t_customer_pass_info';
import { t_agent_info } from '../../utils/db/entities/tables/agent/t_agent_info';
import { Repository } from 'typeorm';
import { UserInfo, LoginFailType, UserType } from './dto/type';
import { AuthDataResponseDTO, AuthResponseDTO } from './dto/reponse';
import Utils from 'src/utils/utils';
import { t_customer_fail_login } from 'src/utils/db/entities/tables/customer/t_customer_fail_login';
import { t_admin_fail_login } from 'src/utils/db/entities/tables/admin/t_admin_fail_login';
import Constants from 'src/utils/constants';

@Injectable()
export class AuthService {

    private readonly logger = new Logger(AuthService.name);
    private utils = new Utils();

    constructor(
        @InjectRepository(t_customer_info)
        private readonly tCustomerInfoRepository: Repository<t_customer_info>,
        @InjectRepository(t_customer_pass_info)
        private readonly tCustomerPassInfo: Repository<t_customer_pass_info>,
        @InjectRepository(t_customer_fail_login)
        private readonly tCustomerFailLoginRepository: Repository<t_customer_fail_login>,
        @InjectRepository(t_agent_info)
        private readonly tAgentInfoRepository: Repository<t_agent_info>,
    ) { }

    async getLogin(req: AuthInput): Promise<AuthResponseDTO> {

        let response: AuthResponseDTO = {
            resultType: false,
            resultMessage: ""
        };

        let resultData: AuthDataResponseDTO;

        let userInfo: UserInfo = await this.getUserInfoByUsername(req.username);
        if (userInfo) {
            let status = (userInfo?.data?.status).toString();
            if (status === "A") {
                this.logger.log('userInfo --> ', JSON.stringify(userInfo, null, 3));
                if (userInfo.data.tel === "A") {

                } else {
                    let customerInfo = userInfo.data as t_customer_info;
                    let password = await this.utils.base64Encode(req.password);

                    let customerPassInfo = await this.getCustomerPassInfoByCustomerIdAndPassword(customerInfo.customer_id, password);
                    this.logger.log('customerPassInfo --> ', JSON.stringify(customerPassInfo, null, 3));

                    if (customerPassInfo) {

                    } else {
                        let loginFail = await this.checkLoginFail(UserType.CUSTOMER, customerInfo.customer_id);
                        this.logger.log('loginFail --> '+  loginFail);
                        if (loginFail === LoginFailType.PASS) {
                            response.resultType = false;
                            response.resultMessage = Constants.MSG_LOGIN_FAIL_WRONG_PASS_WARNING_USER_LOCK;
                        } else {
                            response.resultType = false;
                            response.resultMessage = Constants.MSG_LOGIN_FAIL_USER_LOCK_PLS_CONTACT_AGENT;
                        }
                    }
                }
            } else {
                response.resultType = false;
                response.resultMessage = Constants.MSG_LOGIN_FAIL_USER_LOCK_PLS_CONTACT_AGENT;
            }
        } else {
            response.resultType = false;
            response.resultMessage = Constants.MSG_LOGIN_FAIL_NOT_USER_PLS_REGISTER;
        }

        this.logger.log('response --> ', JSON.stringify(response, null, 3));
        return response;
    }

    private async checkLoginFail(type: UserType, id: number): Promise<LoginFailType> {
        let failLoginData: t_customer_fail_login | t_admin_fail_login;
        let numberFailLogin: number;

        if (type === UserType.CUSTOMER) {
            failLoginData = await this.getCustomerFailLoginByCustomerId(id);
            numberFailLogin = !failLoginData ? 0 : failLoginData.fail_login_total;

            if (numberFailLogin <= 3) {
                await this.getCustomerFailLogin(id, numberFailLogin + 1);
                return LoginFailType.PASS;
            } else {
                return LoginFailType.NOT_PASS;
            }

        } else {

        }
        // return LoginFailType.PASS;
    }

    private async getCustomerFailLogin(customer_id: number, numberFailLogin: number): Promise<void> {
        await this.tCustomerFailLoginRepository.upsert([{ customer_id: customer_id, fail_login_total: numberFailLogin }],
            ['customer_id'])
    }

    private async getCustomerFailLoginByCustomerId(customer_id: number): Promise<t_customer_fail_login> {
        let customerFailLogin: t_customer_fail_login;
        customerFailLogin = await this.tCustomerFailLoginRepository.findOne({
            where: [{
                customer_id: customer_id,
            }]
        })
        return customerFailLogin;
    }

    private async getCustomerPassInfoByCustomerIdAndPassword(customer_id: number, password: string): Promise<t_customer_pass_info> {
        let customerPassInfo: t_customer_pass_info;
        customerPassInfo = await this.tCustomerPassInfo.findOne({
            where: [{
                customer_id: customer_id,
                password: password
            }]
        })
        return customerPassInfo;
    }

    private async getUserInfoByUsername(username: string): Promise<UserInfo> {
        let userInfo: UserInfo = {};
        let customerInfo: t_customer_info;
        let agentInfo: t_agent_info;

        customerInfo = await this.tCustomerInfoRepository.findOne({
            where: [{
                tel: username,
            }]
        })

        if (customerInfo) {
            userInfo = {
                type: UserType.CUSTOMER,
                data: customerInfo
            }
        } else {
            agentInfo = await this.tAgentInfoRepository.findOne({
                where: [{
                    tel: username,
                }]
            })
            if (agentInfo) {
                userInfo = {
                    type: UserType.AGENT,
                    data: customerInfo
                }
            }
        }

        return userInfo
    }

}
