import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthInput } from './dto/request';
import { AuthResponseDTO } from './dto/reponse';
import constants from 'src/utils/constants';

@Controller('/customer-agent/auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('/login')
    async getPosts(@Body() request: AuthInput): Promise<AuthResponseDTO> {

        let response: AuthResponseDTO = {
            resultType: false,
            resultMessage: ""
        };

        if (request?.username && request?.password) {
            let { username, password } = request;

            response = await this.authService.getLogin({
                username: username,
                password: password
            });

        } else {
            response.resultType = false;
            response.resultMessage = constants.MSG_LOGIN_FAIL_NOT_ENTER_USER_OR_PASS;
        }
        
        return response;
    }
}
