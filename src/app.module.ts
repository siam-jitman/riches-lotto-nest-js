import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigService } from './utils/db/config'
import { PostsModule } from './common/posts/posts.module';
import { AuthModule as CustimerAgentAuth } from './customer-agent/auth/auth.module';
import { AuthModule as AdminAgentAuth } from './admin/auth/auth.module';
import { BankModule as BankAdminModule } from './common/bank/bank.module';
import { AgentModule as AdminAgentModule } from './admin/agent/agent.module';
import { entities } from './utils/db/entities';
import { StatusModule } from './common/status/status.module';
import { ServerModule } from './admin/server/server.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot({
      ...TypeOrmConfigService.getTypeOrmOptions()
    }),
    // TypeOrmModule.forFeature(entities),
    PostsModule,
    CustimerAgentAuth,
    AdminAgentAuth,
    BankAdminModule,
    StatusModule,
    ServerModule,
    AdminAgentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
